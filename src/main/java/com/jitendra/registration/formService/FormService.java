package com.jitendra.registration.formService;

import com.jitendra.registration.formModel.Form;
import com.jitendra.registration.formRepository.FormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service
public class FormService {
    @Autowired
    private FormRepository repository;
    public String listUser(Model model){
        List<Form> listUser = repository.findAll();
        model.addAttribute("listUser", listUser);
        return String.valueOf(listUser);
    }

    public Form dataSave(long id, String email, String password, String firstname, String lastname){
        return repository.save(new Form(id, email, password, firstname,lastname));
    }
    public Form dataDelete(long id){
        Form t = repository.findById(id);
       repository.delete(t);
        return t;
    }

    public Form dataUpdate(long id, String email, String password, String firstname, String lastname){
        Form f = repository.findById(id);
        f.setId(id);
        f.setEmail(email);
        f.setFirstName(firstname);
        f.setLastName(lastname);
        f.setPassword(password);
        return repository.save(f);
    }

    public void saveData(Form user) {
         repository.save(user);
    }


}
