package com.jitendra.registration.formController;

import com.jitendra.registration.formModel.Form;
import com.jitendra.registration.formRepository.FormRepository;
import com.jitendra.registration.formService.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller

public class FormController {
    @Autowired
    private FormService service;


    @GetMapping("/register")
    public String showRegistrationForm(Model model) {
        model.addAttribute("user", new Form());

        return "signup_form";
    }

    @PostMapping("/process_register")
    public String processRegister(Form user) {
//        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//        String encodedPassword = passwordEncoder.encode(user.getPassword());
//        user.setPassword(encodedPassword);

        service.saveData(user);

        return "register_success";
    }

    @GetMapping("/users")
    public String getData(Model model) {
//        List<Form> listUsers = repo.findAll();
//        model.addAttribute("listUsers", listUsers);
        service.listUser(model);
        return "test";
    }

//    @GetMapping("/form")
//    public String getForm(){
//        System.out.println("controller call");
//        //return service.getData();
//        return "Hello";
//    }
//
//    @RequestMapping("/form")
//    public String sendData(@RequestParam long id, @RequestParam String email,  @RequestParam("password") String passsword, @RequestParam String firstname, @RequestParam String lastname){
//    Form f = service.dataSave(id, email, passsword, firstname, lastname);
//    return f.toString();
//    }
//    @DeleteMapping("/form/{id}")
//    public Form dataDelete(long id){
//        return service.dataDelete(id);
//    }
//    @PutMapping("/form/{id}")
//    public Form updateValue(@RequestBody long id, @RequestParam String email,  @RequestParam("password") String passsword, @RequestParam String firstname, @RequestParam String lastname){
//        return service.dataUpdate(id, email, passsword, firstname, lastname);
//
//    }

}
//@Controller
//public class FormController {
//
//    @GetMapping("")
//    public String viewHomePage() {
//        return "viewDitails";
//    }
//}

