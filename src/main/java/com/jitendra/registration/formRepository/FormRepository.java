package com.jitendra.registration.formRepository;

import com.jitendra.registration.formModel.Form;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FormRepository extends JpaRepository<Form, Integer> {

   public List<Form> findAll();
   public Form findById(long id);
}
